package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 * @author Carmen Rodriguez (provided the implementation)
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	// This method is complete, but it won't work until the methods it uses are
	// implemented.
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 * @throws FileNotFoundException IOException if something goes wrong with the
	 *                               file I/O.
	 */
	@SuppressWarnings("resource")
	private static VerseList readATV(File bibleFile) {
		// Create a scanner
		//Scanner sc = null;
//		try {
//			// try to create a scanner
//			sc = new Scanner(bibleFile);
//		}
//		// if the file is not found return a VerseList that is null
//		catch (FileNotFoundException e) {
//			e.printStackTrace();
//			VerseList nullList = null;
//			return nullList;
//		}
		try {

			BufferedReader sc = new BufferedReader(new FileReader(bibleFile));
		// declares the empty VerseList to be returned
		VerseList atvList = null;

		// gets the entire first line and sets it as the String first
		String first = sc.readLine();
		// if the first line contains a ":", split the entire string at the ":"
		if (first.contains(":")) {
			String[] firstLine = first.split(":");
			// removes the leading space from the second half of the first line
			String trimmedTitle = firstLine[1].trim();
			// set the VerseList's version and description
			atvList = new VerseList(firstLine[0], trimmedTitle);
		} else if (!first.contains(":")) { // if the first line doesn't have a
											// ":" on the first line, run the
											// else
			// if the first line is empty, set the version to unknown and no
			// description
			if (first.isEmpty()) {
				atvList = new VerseList("unknown", "");
			} else {
				// if the first line isn't empty but there isn't a ":", set the
				// version to the first line and no description
				atvList = new VerseList(first, "");
			}
		}

		// initializes the variables that will be read from each line
		String currLine = "";
		String abb = "";
		int chapter = 0;
		int verseNum = 0;
		String text = "";
		BookOfBible book = null;

		// if there is another line in the file, read it
		 currLine = sc.readLine();
		while (currLine!=null) {

			// The format of each line is BOOK@CHAPTER:VERSE@TEXT
			// Splits at the first ":" only (This is so if there is text with a
			// : it won't split at it
			String[] hold1 = currLine.split(":", 2);
			if (hold1.length < 2) {
				sc.close();
				return null;
			}

			// splits at the first '@' and sets the abbreviation and the chapter
			String[] hold2 = hold1[0].split("@");
			abb = hold2[0];
			if (hold2.length < 2) {
				sc.close();
				return null;
			}
			chapter = Integer.parseInt(hold2[1]);

			// splits at the second '@" and sets the verse number and the text
			String[] hold3 = hold1[1].split("@");
			if (hold3.length < 2) {
				sc.close();
				return null;
			}
			verseNum = Integer.parseInt(hold3[0]);
			text = hold3[1];

			// checks the bible for an abbreviation and sets it to book
			book = BookOfBible.getBookOfBible(abb);
			// if the book finds a correct abbreviation, create a new verse with
			// the found items
			if (book != null) {
				Verse readVerse = new Verse(book, chapter, verseNum, text);
				// adds the created verse to the verseList
				atvList.add(readVerse);
			} else {
				sc.close();
				return null;
//				Verse nullVerse = null;
//				atvList.add(nullVerse);
			}
			// gets the contents of the line to be read
			currLine = sc.readLine();
		}

		// closes the scanner
		sc.close();
		// returns the VerseList
		return atvList;
		} catch(IOException e) {
			return null;
		}
	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	@SuppressWarnings({ "unused", "resource" })
	private static VerseList readXMV(File bibleFile) {
		// TODO Documentation: Stage 8 (Update the Javadoc comment to describe
		// the format of the file.)
		// The XMV is sort of XML-like, but it doesn't have end tags.
		// No description of the file format is given here.
		// You need to look at the file to determine how it should be parsed.
		// Create a scanner
		try {
			FileReader fileReader = new FileReader(bibleFile);
			BufferedReader sc = new BufferedReader(fileReader);
			// gets the entire first line and sets it as the String first
			String abb;
			String title;
			String first = sc.readLine();
			// If the first line contains a : then split the String at that character.
			String[] firstLine = first.split(":", 2);

			if (firstLine.length < 2) {
				if (first.equals("")) {
					abb = "unknown";
					title = "";
				} else {
					abb = firstLine[0].trim();
					title = "";
				}
			} else {
				String[] versionLine = firstLine[0].split(" ");
				abb = versionLine[1].trim();
				title = firstLine[1].trim();
			}
			VerseList xmvList = new VerseList(abb, title);

			// initializes the variables that will be read from each line
			String currLine = "";
			String description = "";
			int chapter = 0;
			int verseNum = 0;
			String verseName = "";
			String chapName = "";
			String text = "";
			BookOfBible book = null;
			
			currLine = sc.readLine();
			// if there is another line in the file, read it
			while (currLine != null) {
				try {
					
					// sets the book name
					if (currLine.startsWith("<Book")) {
						String[] hold1 = currLine.split(",");
						description = hold1[0];
						String[] bookName = description.split("Book");
						String b = bookName[1].trim();

						if (BookOfBible.getBookOfBible(b) == null) {
							return new VerseList("BookError", "");

						} else {
							book = BookOfBible.getBookOfBible(b);
						}
						
					}
					// sets the chapter and chapter number
					else if (currLine.startsWith("<Chapter")) {
						String[] hold2 = currLine.split(" ");
						chapName = hold2[1].replace(">", "").trim();
						chapter = Integer.parseInt(chapName);
					}

					// sets the verse, verse number, and the verseText.
					else if (currLine.startsWith("<Verse")) {
						String[] hold3 = currLine.split(">");
						verseNum = Integer.parseInt(hold3[0].substring(7));
						text = hold3[1].trim();
						Verse readVerse = new Verse(book, chapter, verseNum, text);
						xmvList.add(readVerse);
					}
					
				} catch (RuntimeException e) {
					sc.close();
					e.printStackTrace();
					return null;
				}
				// gets the contents of the line to be read
				currLine = sc.readLine();
				
			}

			// closes the scanner
			sc.close();
			// returns the VerseList
			return xmvList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Note: In the following methods, we should really ensure that the file
	// extension is correct
	// (i.e. it should be ".atv"). However for now we won't worry about it.
	// Hopefully the GUI code
	// will be written in such a way that it will require the extension to be
	// correct if we are
	// concerned about it.

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 */
	@SuppressWarnings("resource")
	public static void writeBibleATV(File file, Bible bible) {
		// TODO Implement me: Stage 8
		// Don't forget to write the first line of the file.
		// HINT: This and the next method are very similar. It seems like you
		// might be
		// able to implement one of them and then call it from the other one.
		try {
			FileWriter outputStream = new FileWriter(file);
			PrintWriter pw = new PrintWriter(outputStream);
			String description = bible.getVersion() + ": " + bible.getTitle();
			VerseList versesOfBible = bible.getAllVerses();
			writeVersesATV(file, description, versesOfBible);
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {

		try {
			FileWriter outputStream = new FileWriter(file);
			PrintWriter pw = new PrintWriter(outputStream);

			pw.println(description);
			for (Verse verse : verses) {
				pw.println(verse.getReference().getBook() + "@" + verse.getReference().getChapter() + ":"
						+ verse.getReference().getVerse() + "@" + verse.getText());

			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		// TODO Implement me: Stage 8
		// This one should be really simple.
		// My version is 4 lines of code (not counting the try/catch code).
		// create a new writer
//	      Writer writer = new PrintWriter(System.out);
		try {
			FileWriter outputStream = new FileWriter(file);
			PrintWriter pw = new PrintWriter(outputStream);

			pw.println(text);
			pw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
