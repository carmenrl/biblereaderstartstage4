package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @editedBy Nazareth Frezghi and Carmen Rodriguez, 2019
 */
public class ArrayListBible implements Bible {

	// ArrayListBible Fields.

	ArrayList<Verse> verses;
	String version;
	String title;

	/**
	 * A new Bible with the given verses.
	 * 
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = verses.copyVerses();

		version = verses.getVersion();

		title = verses.getDescription();

		this.verses.add(new Verse(BookOfBible.Dummy, 1, 1, ""));
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	public int getNumberOfVerses() {
		return verses.size() - 1;
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns whether or not the reference is actually in the Bible
	 * 
	 * @param ref
	 *            the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */
	public boolean isValid(Reference ref) {
		if (ref.getBookOfBible() == BookOfBible.Dummy) {
			return false;
		}
		boolean temp = false;
		for (Verse var : verses) {
			if (var.getReference().getBook().equals(ref.getBook())
					&& var.getReference().getChapter() == ref.getChapter()
					&& var.getReference().getVerse() == ref.getVerse()) {
				temp = true;
			}
		}
		return temp;
	}

	/**
	 * Returns the text of the verse with the given reference
	 * 
	 * @param ref
	 *            the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */
	public String getVerseText(Reference ref) {
		if (isValid(ref)) {
			for (Verse var : verses) {
				if (var.getReference().getBook().equals(ref.getBook())
						&& var.getReference().getChapter() == ref.getChapter()
						&& var.getReference().getVerse() == ref.getVerse()) {
					return var.getText();
				}
			}
		}
		return null;
	}

	/**
	 * Returns a Verse object for the corresponding Reference.
	 * 
	 * @param ref
	 *            A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	public Verse getVerse(Reference ref) {
		for (Verse var : verses) {
			{
				if (var.getReference().equals(ref))
					return var;
			}
		}
		return null;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse
	 *            the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if
	 *         the reference is invalid.
	 */
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		if (ref != null) {
			for (Verse var : verses) {
				if (var.getReference().getBook().equals(ref.getBook())
						&& var.getReference().getChapter() == ref.getChapter()
						&& var.getReference().getVerse() == ref.getVerse()) {
					return var;
				}
			}

		}
		return null;
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------

	/**
	 * Returns a verse list that contains all the verses from the Bible. It
	 * includes version and title.
	 * 
	 * @return a VerseList containing all of the verses from the Bible, in
	 *         order. The version of the VerseList should be set to the version
	 *         of this Bible and the description should be set to the title of
	 *         this Bible.
	 */
	public VerseList getAllVerses() {
		ArrayList<Verse> vs = new ArrayList<>(verses);
		vs.remove(vs.size() - 1);
		return new VerseList(getVersion(), getTitle(), vs);

	}

	/**
	 * Returns a VerseList of all verses containing <i>phrase</i>, which may be
	 * a word, sentence, or whatever. This method just does simple string
	 * matching, so if <i>phrase</i> is <i>eaten</i>, verses with <i>beaten</i>
	 * will be included.
	 * 
	 * @param phrase
	 *            the word/phrase to search for.
	 * @return a VerseList of all verses containing <i>phrase</i>, which may be
	 *         a word, sentence, or whatever. If there are no such verses,
	 *         returns an empty VerseList. In all cases, the version will be set
	 *         to the version of the Bible (via getVersion()) and the
	 *         description will be set to parameter <i>phrase</i>.
	 */
	public VerseList getVersesContaining(String phrase) {
		VerseList v = new VerseList(getVersion(), getTitle());
		for (Verse verse : verses) {
			if (!phrase.equals("")) {
				if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
					v.add(verse);
				}
			}
		}
		return v;
	}

	/**
	 * Returns a ArrayList<Reference> of all references for verses containing
	 * <i>phrase</i>, which may be a word, sentence, or whatever. This method
	 * just does simple string matching, so if <i>phrase</i> is <i>eaten</i>,
	 * verses with <i>beaten</i> will be included.
	 * 
	 * @param phrase
	 *            the phrase to search for
	 * @return a ArrayList<Reference> of all references for verses containing
	 *         <i>phrase</i>, which may be a word, sentence, or whatever. If
	 *         there are no such verses, returns an empty ArrayList<Reference>.
	 */
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> r = new ArrayList<Reference>();
		for (Verse verse : verses) {
			if (!phrase.equals("")) {
				if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
					r.add(verse.getReference());
				}
			}
		}
		return r; 
	}

	/**
	 * Returns a verse list
	 * 
	 * @param references
	 *            a ArrayList<Reference> of references for which verses are
	 *            being requested with each element being the Verse with that
	 *            Reference from this Bible.
	 * @return a VerseList with each element being the Verse with that Reference
	 *         from this Bible, or null if the particular Reference does not
	 *         occur in this Bible. Thus, the size of the returned list will be
	 *         the same as the size of the references parameter, with the items
	 *         from each corresponding. The version will be set to the version
	 *         of the Bible (via getVersion()) and the description will be set
	 *         "Arbitrary list of Verses".
	 */
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verseList = new VerseList(getVersion(), "Arbitrary list of Verses");
		for(Reference r : references)
		{
			verseList.add(getVerse(r));
		}
		return verseList;
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------

	/**
	 * /**
	 * 
	 * @param book
	 *            The book.
	 * @param chapter
	 *            The chapter.
	 * @return the number of the final verse of the given chapter in the given
	 *         book, or -1 if there is a problem (e.g., the book or chapter are
	 *         invalid/null).
	 */
	// @Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		VerseList vList1 = new VerseList(version, title);
		for (int i = 0; i < verses.size(); i++) {
			if (verses.get(i).getReference().getBookOfBible().equals(book)) {
				if (verses.get(i).getReference().getChapter() == chapter) {
					vList1.add(verses.get(i));
				}
			}
		}
		return vList1.get(vList1.size() - 1).getReference().getVerse();
	}

	/**
	 * @param book
	 *            The book
	 * @return the number of the final chapter of the given book, or -1 if there
	 *         is a problem (e.g., the book is invalid/null).
	 */
	// @Override
	public int getLastChapterNumber(BookOfBible book) {
		VerseList vList2 = new VerseList(version, title);
		for (int i = 0; i < verses.size(); i++) {
			if (verses.get(i).getReference().getBookOfBible().equals(book)) {
				vList2.add(verses.get(i));
			}
		}
		return vList2.get(vList2.size() - 1).getReference().getChapter();
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a ArrayList<Reference> of all references in this Bible between
	 *         the firstVerse and lastVerse, inclusive of both; or an empty list
	 *         if the range of verses is invalid in this Bible. An invalid range
	 *         includes if either firstVerse or lastVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0.
	 */
	// @Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<>();
		if (firstVerse.compareTo(lastVerse) <= 0 && isValid(firstVerse) && isValid(lastVerse)) {
			int index = 0;
			while (!verses.get(index).getReference().equals(firstVerse)) {
				index++;
			}
			while (verses.get(index).getReference().compareTo(lastVerse) <= 0) {
				references.add(verses.get(index).getReference());
				index++;
			}
		}
		return references;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a ArrayList<Reference> of all references between the firstVerse
	 *         and lastVerse, exclusive of lastVerse; or an empty list if the
	 *         range of verses is invalid in this Bible. An invalid range
	 *         includes if firstVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0. Notice that lastVerse is
	 *         allowed to be invalid. This can be useful in certain cases to
	 *         find passages more quickly.
	 */
	// @Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<>();
		if (firstVerse.compareTo(lastVerse) <= 0 && isValid(firstVerse)) {
			int index = 0;
			while (!verses.get(index).getReference().equals(firstVerse)) {
				index++;
			}
			while (verses.get(index).getReference().compareTo(lastVerse) < 0) {
				references.add(verses.get(index).getReference());
				index++;
			}
		}
		return references;
	}

	/**
	 * Return a list of all verses from a given book.
	 * 
	 * @param book
	 *            The desired book.
	 * @return a ArrayList<Reference> with version set to this.getVersion(),
	 *         description set to a String representation of <i>book</i>, and
	 *         containing all verses from the given book, in order; or an empty
	 *         list if the book is invalid in this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		ArrayList<Reference> ref = new ArrayList<Reference>();
		for (Verse verse : verses) {
			if (verse.getReference().getBookOfBible().equals(book)) {
				ref.add(verse.getReference());
			}
		}
		return ref;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @return a ArrayList<Reference> containing all references from the given
	 *         chapter of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	// @Override
	
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		return getReferencesExclusive(new Reference(book,chapter, 1), new Reference(book, chapter+1, 1));
//		ArrayList<Reference> ref = new ArrayList<Reference>();
//		for (Verse verse : verses) {
//			if (verse.getReference().getBookOfBible().equals(book)) {
//				if (verse.getReference().getChapter() == chapter) {
//					ref.add(verse.getReference());
//				}
//			}
//		}
//		return ref;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the chapter of the book
	 * @param chapter2
	 *            the chapter of the book
	 * @return a ArrayList<Reference> containing all references from the given
	 *         chapters of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		return getReferencesExclusive(ref, ref2);
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a ArrayList<Reference> containing all references from the given
	 *         passage, in order; or an empty list if the passage is invalid in
	 *         this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);

		return getReferencesInclusive(ref1, ref2);
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the first chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param chapter2
	 *            the second chapter of the book
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a ArrayList<Reference> containing all references from the given
	 *         passage, in order; or an empty list if the passage is invalid in
	 *         this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);

		return getReferencesInclusive(ref1, ref2);
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of the range of verses requested,
	 *         and containing all verses between the firstVerse and lastVerse,
	 *         inclusive of both; or an empty list if the range of verses is
	 *         invalid in this Bible. An invalid range includes if either
	 *         firstVerse or lastVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0.
	 */
	// @Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList vList = new VerseList(version, title);
		for (Verse verse : verses) {
			if (verse.getReference().compareTo(firstVerse) >= 0) {
				if (verse.getReference().compareTo(lastVerse) <= 0) {
					vList.add(verse);
				}
			}
		}
		return vList;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of the range of verses requested,
	 *         with " excluding the final one" tacked onto the end, and
	 *         containing all verses between the firstVerse and lastVerse,
	 *         exclusive of the last one; or an empty list if the range of
	 *         verses is invalid in this Bible. An invalid range includes if
	 *         firstVerse is invalid or if firstVerse.compareTo(lastVerse) > 0.
	 *         Notice that lastVerse is allowed to be invalid. This can be
	 *         useful in certain cases to find passages more quickly.
	 */
	// @Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		VerseList vList = new VerseList(version, title);
		for (Verse verse : verses) {
			if (verse.getReference().compareTo(firstVerse) >= 0) {
				if (verse.getReference().compareTo(lastVerse) < 0) {
					vList.add(verse);
				}
			}
		}
		return vList;
	}

	/**
	 * Return a list of all verses from a given book.
	 * 
	 * @param book
	 *            The desired book.
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of <i>book</i>, and containing all
	 *         verses from the given book, in order; or an empty list if the
	 *         book is invalid in this Bible.
	 */
	// @Override
	public VerseList getBook(BookOfBible book) {
		VerseList vList = new VerseList(version, title);
		for (Verse verse : verses) {
			if (verse.getReference().getBookOfBible().equals(book)) {
				vList.add(verse);
			}
		}
		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the book followed by the chapter
	 *         (e.g. "Genesis 1"), and containing all verses from the given
	 *         chapter of the given book, in order; or an empty list if the book
	 *         is invalid in this Bible.
	 */
	// @Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference reference = new Reference(book, chapter, 1);
		Reference reference2 = new Reference(book, chapter, getLastVerseNumber(book, chapter));
		VerseList vList = getVersesInclusive(reference, reference2);
		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the chapter of the book
	 * @param chapter2
	 *            the chapter of the book
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the book followed by the chapters
	 *         (e.g. "Genesis 1-2"), and containing all verses from the given
	 *         chapters of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	// @Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference reference = new Reference(book, chapter1, 1);
		Reference reference2 = new Reference(book, chapter2, getLastVerseNumber(book, chapter2));
		VerseList vList = getVersesInclusive(reference, reference2);
		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the verses requested (e.g. "Genesis
	 *         1:3-17"), and containing all verses from the given passage, in
	 *         order; or an empty list if the passage is invalid in this Bible.
	 */
	// @Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference r1 = new Reference(book, chapter, verse1);
		Reference r2 = new Reference(book, chapter, verse2);

		VerseList vList = getVersesInclusive(r1, r2);

		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the first chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param chapter2
	 *            the second chapter of the book
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the verses requested (e.g. "Genesis
	 *         1:3-2:17"), and containing all verses from the given passage, in
	 *         order; or an empty list if the passage is invalid in this Bible.
	 */
	// @Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference r1 = new Reference(book, chapter1, verse1);
		Reference r2 = new Reference(book, chapter2, verse2);

		VerseList vList = getVersesInclusive(r1, r2);

		return vList;
	}
}